<?php

  require_once('./Database.php');

  $db = new Database();
  echo $db->isConnected() ? "DB connected." . PHP_EOL : "DB not connected." . PHP_EOL;

  if (!$db->isConnected() ) {
    echo $db->getError();
    die('Unable to connect to database.');
  }

  $db->query("SELECT * FROM " . $db->getTableName());
  var_dump($db->resultset());
  echo "Rows: " . $db->rowCount() . PHP_EOL;

  $db->query("SELECT * FROM " . $db->getTableName() . " WHERE id = :id");
  $db->bind(':id', 2);
  var_dump($db->single());
?>
